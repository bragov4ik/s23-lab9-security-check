# Homework solution

I chose site https://vndb.org/

## Forgot password

| Test step | Result |
|---|---|
| Open main page | `/` is shown |
| Press 'Login' button | Login page opened |
| Press 'Forgot password' | Form for password recovery is demonstrated |
| Type unregistered email, submit | 'Unknown email address.' |
| Type registered email, submit | Success message, instructions were sent. |

Random email:
![unknown address](https://i.imgur.com/mJpzUJa.png)

Registered email:
![success](https://i.imgur.com/DAHXHeu.png)

User enumeration attack is possible via 'Forgot password' feature.
[OWASP-recommended procedure is not followed](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-request)

## User info access

User profiles in the website are accessed via https://vndb.org/uXXX pattern, where 'XXX' is some number.

We focus on IDOR cheatsheet: https://cheatsheetseries.owasp.org/cheatsheets/Insecure_Direct_Object_Reference_Prevention_Cheat_Sheet.html

| Test step | Result |
|---|---|
| Open main page | Opens main page |
| Click on username of the first entry in 'Recent changes' | Opened user profile page https://vndb.org/u24907 |
| Try to alter the number slightly and compare registration dates | u24906, u24907, u24908 all were registered in 2012-04-01 |
| It seems that they're given out sequentially, try to access id 1 | https://vndb.org/u1 is a valid user profile |
| Search for more recently-registered users in main page | Found https://vndb.org/u238767 registered this year |
| Try https://vndb.org/u240000 now | Also a valid user |
| Try https://vndb.org/u250000 | Not found |

![u240000](https://i.imgur.com/3uPvPGS.png)

As a result, we found that user ids are given out sequentially. It means that an attacker knows how many users are registered (240k-250k, can easily find precise number) and can perform user enumeration attack.

## Token Sidejacking

[Cheat sheet](https://cheatsheetseries.owasp.org/cheatsheets/JSON_Web_Token_for_Java_Cheat_Sheet.html#token-sidejacking)

| Test step | Result |
|---|---|
| We want to check headers for token cookie. Visit login page | Login page is opened |
| Open Firefox network monitor, set 'Persist logs' setting | Network activity is started tracking |
| Type login and password. Press 'Submit' | Logged in successfully. Network activity started filling up |
| Find response with auth token. Check cookie parameters | Found cookie. HttpOnly + Secure + SameSite are set, Max-Age + cookie prefixes are not. |

Network activity monitor

![Network monitor setting](https://i.imgur.com/R99ha9o.png)

I won't include token due to security reasons. Anyway the settings are not perfect according to the cheatsheet, however they should be enough for such a simple website.

## Implement Proper Password Strength Controls

[Cheat sheet](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#implement-proper-password-strength-controls)

| Test step | Result |
|---|---|
| Start account registration, follow link from email | Page for opening password is opened |
| Try to input 1-character password and submit | Reported that min length is 4 symbols. Too weak limitation according to the cheatsheet |
| Try to use very long password containing unicode characters and whitespaces (259 characters, `┡   ⃅⇡⮊⬌⬄⑿⥣⻸⇜␏⦼ↂ❜♚⏝⇅☉␺₉⦡╋⃹⻵⮯⭌╦⛣⢱▌⓿⣌⾨⩮₋❥⤚⊫⒮⋄⁋↰⤹ⷞ⑩⺦⥨⬔↎⊖⮯⍙ↆ⿆⟭⒠◊♴⫆⌇⅞╚Ⓜ▖⃎⫤⑎☈ℤ⭡⸊⛠║⺌⍈⮤–⃳⅄⏃▀⿸⯮⪒✛╋⿍⥩␤⩷⊸◒➒≗╧⺋⾞⦜⊮⼡ℊ⿽◾Ⅾ⡎≛⎣⼏➇⦏⇄⦭⵹⇢⊌⟖⸡⭊✓⫯ⲽ⺔⳺⚙₝ⓡ⿪⫁₂⊅⬯⪠⬄╛⧰∑⍛⹩⻂⫬Ⅳ⏧⩕⇯⫺⬛Ⲷ⌆Ⳉ┿⠯…⎵❍⛯⧴⹗⧻ⶃ⋇Ⅹ↓⇖⩒⇷❷┰⬜ⶹⓋ⌰⅌ↄ⋃␣⼉⢈⁅⽫⚿Ⲣⴀ⒙⩻∔⹂⊇ⴰ⪰⣿◑₣⨋✳↣ⓡ␒♋⇝⸃⩃⒯ⶎⶇ≃⹵⬾⡡⪈ⷵ⏶⏊⒋▌⓵Ⰾ⫗⛺▵⊎ⵍⲤ⑟ⴿ⬗⇥⣷ℙ⨛◭⟊ⓜ⭧ⶍ⃉⢞⋐‾‒⸍⽱␮Ⰲ‟⦞↛ⶐ⑙≔ⰰⵞ⤢▰ Ɐⷲ`; probably some were inserted incorrectly here. you get the spirit) | Allowed registration |
| Logout and log back in to check silent truncation. | Successfully logged in |
| Open form for changing password, try to set common/leaked password `password` | Reported as 'leaked password', did not allow |
| Try to set 4-character password to verify that 4 character limit is followed | It allowed password `ΞΞΞΞ` |

Overall the only violation is minimum password length of 4 characters. According to OWASP recommendations it should be increased to 8.

Password rotation is not feasible/possible to check within the scope of this HW.
